package com.kristiawan.springmvcsetup.dao;

import java.util.List;

public interface BaseDao<T> {
    T save(T entity);

    T update(T entity);

    List<T> find();

    T findById(long id);

    T delete(long id);
}

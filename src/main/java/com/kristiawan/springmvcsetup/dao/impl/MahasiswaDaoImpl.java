package com.kristiawan.springmvcsetup.dao.impl;

import com.kristiawan.springmvcsetup.dao.MahasiswaDao;
import com.kristiawan.springmvcsetup.entity.Mahasiswa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MahasiswaDaoImpl implements MahasiswaDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Mahasiswa save(Mahasiswa entity) {
        String sql = "INSERT INTO MAHASISWA VALUES(?, ?, ?) ";

        List<Object> params = new ArrayList<Object>();
        params.add(entity.getId());
        params.add(entity.getNim());
        params.add(entity.getNama());

        jdbcTemplate.update(sql, params);
        return entity;
    }

    public Mahasiswa update(Mahasiswa entity) {
        return null;
    }

    public List<Mahasiswa> find() {
        String sql = "SELECT * FROM MAHASISWA ";

        return jdbcTemplate.query(sql, new RowMapper<Mahasiswa>() {
            public Mahasiswa mapRow(ResultSet resultSet, int i) throws SQLException {
                Mahasiswa mahasiswa = new Mahasiswa();
                mahasiswa.setId(resultSet.getLong("id"));
                mahasiswa.setNim(resultSet.getString("nim"));
                mahasiswa.setNama(resultSet.getString("nama"));
                return mahasiswa;
            }
        });
    }

    public Mahasiswa findById(long id) {
        return null;
    }

    public Mahasiswa delete(long id) {
        return null;
    }
}

package com.kristiawan.springmvcsetup.dao;

import com.kristiawan.springmvcsetup.entity.Mahasiswa;

public interface MahasiswaDao extends BaseDao<Mahasiswa> {
}

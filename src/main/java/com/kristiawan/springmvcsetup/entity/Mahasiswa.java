package com.kristiawan.springmvcsetup.entity;

public class Mahasiswa {
    private long id;
    private String nim;
    private String nama;

    public Mahasiswa() {
    }

    public Mahasiswa(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
